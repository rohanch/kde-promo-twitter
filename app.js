const needle = require("needle");
var fs = require("fs").promises;
const dotenv = require("dotenv");
var parse = require("csv-parse/lib/sync");
var stringify = require("csv-stringify");
const cron = require("node-cron");
const os = require("os");

dotenv.config();

const endpointURL = "https://api.twitter.com/2/users/by?usernames=";
const token = process.env.BEARER_TOKEN;

const mURL = "https://mastodon.technology/api/v1/accounts/42572";

async function getTwitter() {
	const params = {
		usernames: "kdecommunity",
		"user.fields": "public_metrics",
	};

	const res = await needle("get", endpointURL, params, {
		headers: {
			authorization: `Bearer ${token}`,
		},
	});

	if (res.body) {
		return res.body;
	} else {
		throw new Error("Unsuccessful request");
	}
}

async function getMastodon() {
	const res = await needle("get", mURL);
	if (res.body) {
		return res.body;
	} else {
		throw new Error("Mastodon Unsuccesful Request");
	}
}

async function writeToCsv() {
	try {
		const response = await getTwitter();
		const public_metrics = response.data[0].public_metrics;
		var num_followers = public_metrics.followers_count;
		var tweet_count = public_metrics.tweet_count;
		//read file to get last count
		const fileContent = await fs.readFile(__dirname + "/test.csv");
		const records = await parse(fileContent, { columns: true });
		var last_record = records[records.length - 1];
		console.log(last_record);
		var increment_in_followers =
			num_followers - parseInt(last_record["num_followers"]);
		var increment_in_posts =
				tweet_count - parseInt(last_record["num_posts"]),
			event = new Date();
		var date = event.toDateString();
		//mastodon
		var metrics = await getMastodon();
		var MFollowers = metrics.followers_count;
		var MTweets = metrics.statuses_count;
		var Mincrement_in_followers =
			MFollowers - parseInt(last_record["mastodon_followers"]);
		var Mincrement_in_toots = MTweets - parseInt(last_record["num_toots"]);

		var new_record = {
			date: date,
			num_followers: num_followers,
			increment_in_followers: increment_in_followers,
			num_posts: tweet_count,
			increment_in_posts: increment_in_posts,
			users_per_tweets: increment_in_followers / increment_in_posts,
			mastodon_followers: MFollowers,
			mastodon_increment_in_followers: Mincrement_in_followers,
			num_toots: MTweets,
			increment_in_toots: Mincrement_in_toots,
			users_per_toots: Mincrement_in_followers / Mincrement_in_toots,
		};
		console.log(new_record);
		stringify(
			[new_record],
			{
				header: false,
			},
			function (err, output) {
				if (err) {
					console.log(err);
					process.exit(-1);
				}
				fs.appendFile(__dirname + "/test.csv", output);
			}
		);
	} catch (e) {
		console.log(e);
		process.exit(-1);
	}
}

cron.schedule("0 0 * * 0", async function () {
	console.log("running task every week");
	try {
		await writeToCsv();
		console.log("Contents written to file");
	} catch (e) {
		console.error(e);
		process.exit(-1);
	}
});
